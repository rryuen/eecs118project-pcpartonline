<!-- This is the main index page -->

<!DOCTYPE html>
<html lang="en">

	<head>
		<?php include "includes/header.php";?>
	</head>
  
	<body>

		<?php include "includes/navbar.php";?>

		<div id="myCarousel" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#myCarousel" data-slide-to="1"></li>
				<li data-target="#myCarousel" data-slide-to="2"></li>
				<li data-target="#myCarousel" data-slide-to="3"></li>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<div class="item active">
				  <img src="images/pic1.jpg" alt="Image1">
				</div>

				<div class="item">
				  <img src="images/pic2.jpg" alt="Image2">
				</div>

				<div class="item">
				  <img src="images/pic3.jpg" alt="Image3">
				</div>

				<div class="item">
				  <img src="images/pic4.jpg" alt="Image4">
				</div>
			</div>

			<!-- Left and right controls -->
			<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
			</a>
		</div> <!--end carousel-->
		<br>

		<div class="container">
	
		   <?php
			if (isset($_GET['submit'])) {
				if (empty($_GET['searchInput'])) {  	
					echo "<h4 align='center'>Type something in the search box <br> OR <br> Use the dropdown menu to browse by Parts or by Brand.<br> </h4>"; 
				}
				else  {
					echo "<h4 align='center'>Displaying: ". strtoupper($_GET['searchInput']) . "</h4>";
					$search = $_GET['searchInput'];
					include "filtertable.php";
				}
			}
			elseif ($_GET['listValue']) {
				echo "<h4 align='center'>Displaying: ". strtoupper($_GET['listValue']) . "</h4>";		
				$search = $_GET['listValue'];
				include "filtertable.php";
			}
			else {
				echo "<h4 align='center'>Use the <strong>Dropdown Menu</strong> or <strong>Search Box<strong> to search for items.</h4>";
			}
			?>	
	
		</div> <!--end container -->

		<div class="footer">

			<p></br><center>PC Parts Online | Copyright (c) 2016<center></p>

		</div>

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="js/bootstrap.min.js"></script>

	</body>

</html>

