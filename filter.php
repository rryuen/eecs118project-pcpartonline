<!-- This builds the price filter dropdown. -->

<form  id="form" name="form" method="get">
	
	<?php
	//if dropdown menu is used (by part or by brand)
	if($_GET['listValue']){
		$param = $_GET['listValue'];
	}
	
	//if searchbox is used
	if($_GET['searchInput']) {
		$param = $_GET['searchInput'];
	}
	?>
	
	<input type="hidden" name="listValue" value="<?php echo $param; ?>"/>
	<br>Price Range
	<select id="priceOpt" name="priceOpt" onchange="this.form.submit();" class="">
		<option value="1">All</option>
		<option value="2">$0-50</option>
		<option value="3">$51-100</option>
		<option value="4">$100+</option>
	</select>

	</br></br>
	
</form>
	//displays the currently selected option from the dropdown
	<script type="text/javascript">
		document.getElementById('priceOpt').value = "<?php echo $_GET['priceOpt'];?>";
	</script>

	<?php
	
	//initialize min & max
	$min = 1; $max = 1000000000;

	//assign new values for min & max depending on selected dropdown option
	switch($_GET['priceOpt']) {	
		case "1":
			$min = 1; $max=1000000000;
			break;
		case "2":
			$min = 1; $max=50;
			break;
		case "3":
			$min = 51; $max=100;
			break;
		case "4":
			$min = 100; $max=1000000000;
			break;
	}
		
			