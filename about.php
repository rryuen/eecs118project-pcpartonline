<!-- About Page -->

<!DOCTYPE html>
<html lang="en">
  <head>
	<?php include "includes/header.php";?>

  </head>
  
  <body>
  
	<?php include "includes/navbar.php";?>
   	
  	<div class="container">

		<h3> About </h3>
		<br>
		<p>
		<h4>This website was created as Final Project for EECS118.<br> </h4>  
		<br>
		<p>PC Parts Online mimics an Amazon affiliate website. Search parts and add them to your Amazon cart with a push of a button. 
			As an added feature, each item displayed has a button that allows you to compare prices with eBay, Newegg, and BestBuy. 
			This allows the user to find the cheapest price for an item.</p>  
		</p>
			
    </div> <!--end container -->

	<div class="footer">
	
		<p></br><center>PC Parts Online | Copyright (c) 2016<center></p>
	
	</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

  </body>
  
</html>

